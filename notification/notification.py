import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask, request,make_response
import smtplib
from email.mime.text import MIMEText
from email.header  import Header

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
SMTP_SERVER = "smtp.gmail.com" if (os.environ.get("SMTP_SERVER") is None) else os.environ.get("SMTP_SERVER")
SMTP_PORT = "587" if (os.environ.get("SMTP_PORT") is None) else os.environ.get("SMTP_PORT")
SMTP_LOGIN_ADDRESS = "changeme" if (os.environ.get("SMTP_LOGIN_ADDRESS") is None) else os.environ.get("SMTP_LOGIN_ADDRESS")
SMTP_LOGIN_PASSWORD = "changeme" if (os.environ.get("SMTP_LOGIN_PASSWORD") is None) else os.environ.get("SMTP_LOGIN_PASSWORD")
FROM_ADDRESS = "changeme" if (os.environ.get("FROM_ADDRESS") is None) else os.environ.get("FROM_ADDRESS")
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
MAIL_SUBJECT = "Bookinfo Notification" if (os.environ.get("MAIL_SUBJECT") is None) else os.environ.get("MAIL_SUBJECT")

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# メール送信
def notification( _product_id: int, _state: str):
    # メール件名
    subject = MAIL_SUBJECT

    # メール本文
    MAIL_BODY = f'product_id {_product_id}, status: {_state}' if (os.environ.get("MAIL_BODY") is None) else os.environ.get("MAIL_BODY")

    # MIMETextオブジェクトの生成
    charset = 'iso-2022-jp'
    msg = MIMEText(MAIL_BODY, 'plain', charset)  
    msg['Subject'] = Header(subject, charset)
    msg['From'] = FROM_ADDRESS 
    msg['To'] = SMTP_LOGIN_ADDRESS 

    # SMTPサーバへ接続
    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    # ログイン
    server.login(SMTP_LOGIN_ADDRESS, SMTP_LOGIN_PASSWORD)
    # メール送信
    server.send_message(msg)
    server.quit()


# クラウドイベントの受信
@app.route("/", methods=["POST"])
def recieve_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")

    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    # メール送信
    notification(_product_id, _state)

    # 別のイベントで応答
    event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': 'NOTIFICATION_SUCCESS', 'user': _user }
    response = make_response( json.dumps(value).encode('utf-8') )
    response.headers["Ce-Id"] = str(uuid.uuid4())
    response.headers["Ce-Source"] = "dev.knative.serving#notification"
    response.headers["Ce-specversion"] = "1.0"
    response.headers["Ce-Type"] = "cloudevent.event.type"
    return response

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))