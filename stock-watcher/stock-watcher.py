import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask, request,make_response

# DB modules
from model import Stock
from db import db_session

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)


# 在庫数を返却する
def count(_product_id: int, _state: str) -> None:
    stock = db_session.query(Stock).filter(Stock.product_id == _product_id).first()
    _state = f"STOCK NUMBER: {stock.count}"
    db_session.commit()
    return _state

# クラウドイベントの受信
@app.route("/", methods=["POST"])
def recieve_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")

    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    _state = count(_product_id, _state)

    # 別のCloudEvents形式のイベントデータをHTTPレスポンスとして応答
    event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
    response = make_response( json.dumps(value).encode('utf-8') )
    response.headers["Ce-Id"] = str(uuid.uuid4())
    response.headers["Ce-Source"] = "dev.knative.serving#stock-watcher"
    response.headers["Ce-specversion"] = "1.0"
    response.headers["Ce-Type"] = "cloudevent.event.type"
    return response

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))